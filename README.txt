This README is for the Latte Tutorial App.  Upon startup the user sees a Welcome screen with a background of latte art and a button at the bottom of the screen that says "Let's go".  Pushing the button starts the lesson. The Welcome screen also has a TextView that says "Good Morning!", "Good Afternoon!" or "Good Evening!", depending on the time of day according to the device.  This is implemented by getting the hour and then using if-else logic.

There are 5 separate steps (or activities) to teach the user how to make a latte, these are:
1) Preparation
2) Grind, Dose and Puck
3) Pulling the shot
4) Steaming the milk
5) Pouring the milk

In each of the steps above, I have provided pictures to compliment the text.  Clicking on each picture will expand the picture for a better view.  At the bottom of each page is a "back" and "next" button.  Clicking on the back button takes the user back to the previous screen.  Clicking the next button takes the user to the next step.

Throughout the app there is an appbar at the top.  On the left side of the appbar is the title of the step the user is on.  On the right side of the appbar is the overflow menu that contains an 'About' page.  The About page thanks the user for using the app, lists me as the developer and also provides my email address.  The appbar within the About page also features an "up" button on the far left.  Clicking this button takes the user back to whatever step they were on when they entered the "About" screen.  The "about" screen was not part of my original project idea.

My orginal project idea included having a slpash screen and a video of each step that would play at the bottom of each page.  In the interest of time, I cut these features from the final app.

Thanks again for using Latte Tutorial!

-Troy Braswell




