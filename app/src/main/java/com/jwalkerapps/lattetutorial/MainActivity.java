package com.jwalkerapps.lattetutorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {


    /**
     * This activity displays a welcome screen.
     * First things first, lets display the activity and setup the app bar
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        /**
         * This bit of code determines what hour it currently is and displays "Good Morning!", "Good Afternoon!" or "Good Evening!" on the Welcome screen depending on the current hour.
         * @param hour - this is the hour according to the device the app is running on
         */
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

        // If the time is before noon, the say Good Morning
        if (hour < 12) {
            TextView statusTextView = (TextView) (findViewById(R.id.good_day));
            statusTextView.setText(getString(R.string.good_morning));
            // If the time is after 5pm then say Good Evening
        } else if (hour >= 17){
            TextView statusTextView = (TextView) (findViewById(R.id.good_day));
            statusTextView.setText(getString(R.string.good_evening));
            // From noon to 4:59pm, say Good Afternoon
        } else {
            TextView statusTextView = (TextView) (findViewById(R.id.good_day));
            statusTextView.setText(getString(R.string.good_afternoon));
        }
    }

    /**
     * This method initializes the contents of the options menu, adding the overflow dots
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.toolbar, menu);
        return true;
    }

    /**
     * This method is called when the user chooses an option within the appbar's overflow
     * Currently there is only one option - 'About', but with the switch setup, more can be added later
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                Intent intentAbout = new Intent(this, AboutActivity.class);
                startActivity(intentAbout);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    /*This method is called when the 'Let's Go' button is pressed*/
    public void letsGo(View view) {
        Intent intentForwardPrep = new Intent(this, PrepActivity.class);
        startActivity(intentForwardPrep);
    }
}
