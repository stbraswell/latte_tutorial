<resources>
    <string name="app_name">Latte Tutorial</string>

    <!--Toolbar-->
    <string name="action_about">About</string>

    <!--Welcome Screen-->
    <string name="welcome">Welcome</string>
    <string name="good_morning">Good Morning!</string>
    <string name="good_afternoon">Good Afternoon!</string>
    <string name="good_evening">Good Evening!</string>


    <!--prep_layout-->
    <string name="appbar_text_prep">Step 1: Prep </string>
    <string name="know_the_machine">Know The Machine</string>
    <string name="grouphead">1) Grouphead</string>
    <string name="grouphead_description">The grouphead is where the heated brew water comes out of.  This is also where the portafilter is secured to.</string>
    <string name="steam_wand">2) Steam Wand</string>
    <string name="steam_wand_description">The steam wand is used to steam/froth the milk for the latte.</string>
    <string name="service_water_wand">3) Service Water Wand</string>
    <string name="service_water_wand_description">The service water wand is used to deliver hot water for warming cups or rinsing.</string>
    <string name="steam_valve">4) Steam Valve</string>
    <string name="steam_valve_description">The steam valve is used to open or close the flow of steam exiting the steam wand.</string>
    <string name="service_water_valve">5) Service Water Valve</string>
    <string name="service_water_valve_description">The service water valve is used to open or close the flow of service water exiting the service water wand.</string>
    <string name="portafilter">6) Portafilter</string>
    <string name="portafilter_description">The portafilter holds the coffee grounds and attaches to the grouphead.</string>
    <string name="hdr_tools_list">What you\'ll need</string>
    <string name="tool_list_1">1) Espresso Machine</string>
    <string name="tool_list_2">2) Fresh roasted coffee beans</string>
    <string name="tool_list_3">3) Burr grinder</string>
    <string name="tool_list_4">4) Milk</string>
    <string name="tool_list_5">5) Frothing pitcher</string>
    <string name="tool_list_6">6) Thermometer</string>
    <string name="tool_list_7">7) Coffee cup</string>
    <string name="tool_list_9">9) Sugar (optional)</string>
    <string name="tool_list_8">8) Scale</string>

    <!--puck_layout-->
    <string name="appbar_text_puck">Step 2: Grind, Dose and Puck</string>
    <!--<string name="hdr_puck">Step 2: Grind, Dose and Puck</string>-->
    <string name="puck_list_1">1) Place beans in hopper of grinder.</string>
    <string name="puck_list_2">2) Adjust grind setting such that the grounds feel and look like fine sugar granuals.  This step may take some trial and error.</string>
    <string name="puck_list_3">3) Grind beans until there are about 20g in the portafilter.  I like a bit more in mine!</string>
    <string name="puck_list_4">4) Gently tap the portafilter on the counter to settle the grinds.</string>
    <string name="puck_list_5">5) Level the grinds in the portafilter.</string>
    <string name="puck_list_6">6) Place the tamper on top of the grinds and firmly push down.  Keep the surface of the grinds level.</string>
    <string name="puck_list_7">7) Release pressure of tamper and lightly spin the tamper on top of the grinds to "polish" the puck.</string>
    <string name="puck_list_8">8) Great Job!</string>

    <!--shot_puller-->
    <string name="appbar_text_shot">Step 3: Pulling the shot</string>
    <!--<string name="hdr_pull_shot">Step 3: Pulling the shot</string>-->
    <string name="shot_pull_1">1) Activate pump to run water through the grouphead for 2 seconds.</string>
    <string name="shot_pull_2">2) Insert portafilter in grouphead, then twist (typically left to right) to tighten/lock in place.  This should\'t require much force at all.</string>
    <string name="shot_pull_3">3) Place your coffee cup on the scale and tare scale to 0g.</string>
    <string name="shot_pull_4">4) Place your coffee cup under the portafilter.</string>
    <string name="shot_pull_5">5) Activate pump to begin extraction</string>
    <string name="shot_pull_5a">* A typical double shot of espresso will take about 20-25 seconds.  Use a timer or count it out \"One Mississippi....\"</string>
    <string name="shot_pull_6">6) Stop extraction.  Place cup with espresso on the scale.  Depending on preference, you want about double or triple the weight of the coffee grounds.  So if you used 20g of coffee, then you\'re looking for around 40g of espresso.</string>

    <!--milk_steaming-->
    <string name="appbar_text_steam">Step 4: Steam the Milk</string>
    <!--<string name="hdr_steam">Step 4: Steam the Milk</string>-->
    <string name="steam_1">1) Pour cold milk into frothing pitcher.</string>
    <string name="steam_2">2) Add sugar to milk, to taste. (Optional)</string>
    <string name="steam_3">3) Place thermometer in frothing pitcher.</string>
    <string name="steam_4">4) Point steam wand into drip tray.</string>
    <string name="steam_5">5) Open steam valve for ~2 seconds to clear it out.</string>
    <string name="steam_6">6) Move steam wand back to the side of the machine, angle it slightly towards you.</string>
    <string name="steam_7">7) Place the tip of the steam wand about 1/2 inch below the surface of the milk.</string>
    <string name="steam_8">8) Fully open the steam valve.</string>
    <string name="steam_9">9) Keep the steam wand between the center and side of the pitcher.</string>
    <string name="steam_10">10) Slowly raise or lower the pitcher, just slightly, until the steam and milk begin to make a vaccuum noise.  This is a good sound, it is adding air into the milk and allowing for the creation of \"micorfoam\"</string>
    <string name="steam_11">11) Keep an eye on the thermometer.  Once the temperature reaches ~115 degrees F, submerge the steam wand into the milk but do not hit the bottom of the pitcher.</string>
    <string name="steam_12">12) Just before the temperature reaches 150 degrees F, close the seam valve and then remove the steam wand from the pitcher.</string>
    <string name="steam_13">13) The milk should look glossy and have very tiny bubbles on the surface.</string>


    <!--milk_pour-->
    <string name="appbar_text_pour">Step 5: Pour the Milk</string>
    <!--<string name="hdr_pour">Step 5: Pour the Milk</string>-->
    <string name="pour_1">1) If the milk has larger bubbles, tap the bottom of the pitcher on the counter to pop them.</string>
    <string name="pour_2">2) Place the cup of espresso on the counter in front of you.</string>
    <string name="pour_3">3) Swirl the milk in the pitcher.</string>
    <string name="pour_4">4) Begin to pour the milk into the espresso about 1/2 inch from the side of the cup.</string>
    <string name="pour_5">5) While continuing to pour, move the pitcher towards the other side of the cup.</string>
    <string name="pour_6">6) Once you reach the other side of the cup, slightly tilt the pitcher to increase the flow.  At this point you should see the \"microfoam\" coming out and staying on the surface of the espresso.</string>
    <string name="pour_7">7) Begin waving the pitcher, side to side, while slowly moving back to the other side of the cup..</string>
    <string name="pour_8">8) Once you reach the other side of the cup, quickly move the pitcher to the other side of the cup in a straight line..</string>
    <string name="pour_9">9) With practice, you will be able to make a great design!</string>

    <!--about page-->
    <string name="appbar_text_About">About</string>
    <string name="thanks">Thank You for using my Latte Tutorial!</string>
    <string name="developed_by">Developed by: Troy B.</string>
    <string name="email_me">Email me: jwalkerapps@gmail.com</string>


</resources>
